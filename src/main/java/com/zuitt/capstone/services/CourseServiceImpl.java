package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    // Create course
    public void createCourse(Course course) {
        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        courseRepository.save(newCourse);
    }

    // Get all course
    public Iterable<Course> getCourses() {
        return courseRepository.findAll();
    }

    public ResponseEntity deleteCourse(Long id) {
        courseRepository.deleteById(id);
        return new ResponseEntity<>("Course deleted successfully", HttpStatus.OK);
    }

    public ResponseEntity updateCourse(Long id, Course course) {
        Course courseForUpdating = courseRepository.findById(id).get();

        courseForUpdating.setName(course.getName());
        courseForUpdating.setDescription(course.getDescription());
        courseForUpdating.setPrice(course.getPrice());

        courseRepository.save(courseForUpdating);
        return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
    }

}
