package com.zuitt.capstone.repositories;

import com.zuitt.capstone.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Object>{
}
