package com.zuitt.capstone.models;

import javax.persistence.*;
import java.util.Set;

// Marks this Java object as a representation of an entity/record from the database table "posts"
@Entity
// Designate the table name related to the model
@Table(name="courses")
public class Course {

    // Properties
    // Indicates that this property represents the primary key of the table
    @Id
    // Values for this property will be auto-incremented
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Class properties that represent table columns in a relational database are annotated as @Column
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Double price;

    // Constructors
    // Default constructors are required when retrieving data from the database
    public Course(){}

    public Course(String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
